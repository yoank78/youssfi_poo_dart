import 'model/circle.model.dart';
import 'model/geometric.design.model.dart';
import 'model/point.model.dart';
import 'model/rectangle.model.dart';

void main() {
  print("Test4");

  GeometricDesign geometricDesign = GeometricDesign();
  geometricDesign.add(Circle(p1: Point(x: 10, y: 10), p2: Point(x: 60, y: 60),));
  geometricDesign.add(Circle(p1: Point(x: 50, y: 60), p2: Point(x: 160, y: 90),));
  geometricDesign.add(Rectangle(p1: Point(x: 33, y: 44), p2: Point(x: 23, y: 98),));
  geometricDesign.show();
  print('****************************************');
  print(geometricDesign.toJson());
  geometricDesign.save('design.json');
}