import 'dart:convert';

import './model/point.model.dart';

void main() {
  print("Test2");

  Point p1 = const Point(x: 10, y: 20);
  Point p2 = const Point(x: 10, y: 20);

  print("p1=${p1.toString()}");
  print("p2=${p2.toString()}");
  // Comparaison adresse mémoire des objets
  print("p1==p2 : ${p1==p2}");
  print("p1==p2 : ${identical(p1, p2)}");

}