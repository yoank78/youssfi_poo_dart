import 'dart:convert';

import './model/point.model.dart';

void main() {
  print("Test");

  Point p1 = new Point(x: 10, y: 20);
  print(p1.x);
  print(p1.y);
  print("p1=${p1.toString()}");

  Point p2 = Point.fromArray([23.0, 12.0]);
  print("p2=${p2.toString()}");

  print("distance entre p1 et p2 = ${p1.distanceTo(p2)}");
  print("distance entre p1 et p2 = ${p1.distanceTo(p2).toStringAsFixed(2)}");


  print("p1 en JSON:");
  // Fromat Map
  print(p1.toJson());
  // Format JSON
  print(json.encode(p1.toJson()));
  JsonEncoder jsonEncoder = JsonEncoder.withIndent("  ");
  // Format JSON avec indentation
  print(jsonEncoder.convert(p1.toJson()));


  // Point p3 = Point.fromJson({'x':34.0, 'y':55.0});
  // print("p3=${p3.toString()}");

  Point p4 = Point.fromJson({'x':34, 'y':55});
  print("p4=${p4.toString()}");

  Map<String, dynamic> data = new Map();
  data['x'] = 77;
  data['y'] = 15;
  Point p5 = Point.fromJson(data);
  print("p5=${p5.toString()}");

}