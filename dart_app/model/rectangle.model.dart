
import 'dart:math';

import 'shape.model.dart';

class Rectangle extends Shape {
  
  // Circle({required super.p1, required super.p2});
  Rectangle({required p1, required p2}) : super(p1: p1, p2: p2);


  double getWidth() {
    // print("DEBUG : getRadius=${p1.distanceTo(p2)}");
    return (p2.x - p1.x).abs();
  }

  double getHeight() {
    // print("DEBUG : getRadius=${p1.distanceTo(p2)}");
    return (p2.y - p1.y).abs();
  }

  @override
  double getArea() {
    return getWidth()*getHeight();
  }

  @override
  double getPerimeter() {
    return 2*(getWidth() + getHeight());
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      'type':'Rectangle',
      'x': p1.x,
      'y': p1.y,
      'width': getWidth(),
      'height': getHeight()
    };
  }

}