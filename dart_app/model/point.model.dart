import 'dart:math';

class Point {
  final double x;
  final double y;

  // Définition d'attributs
  // avec final par défaut les attributs sont obligatoires. Avec le ? ils deviennent optionnels
  // final double? x;
  // final double? y;

  // Point(this.x, this.y);

  // Constructeur avec argument nommé
  // le mot clé required rend les attributs obligatoires
  // Point({required this.x, required this.y});
  // Constructeur const
  const Point({required this.x, required this.y});

  // Constructeurs nommés (fabrique)
  Point.fromArray(List<double> data) : 
      x = data[0], y = data[1];

  Point.fromJson(Map<String, dynamic> data) :
      x = (data['x'] as num).toDouble(), y = (data['y'] as num).toDouble();

  // Attributs privés avec getter et setter
  // final double _x;
  // final double _y;
  // double get x => _x;
  // set x(double value) {
  //   _x = value;
  // }
  // double get y => _y;
  // set y(double value) {
  //   _y = value;
  // }

  double distanceTo(Point p) {
    double deltaX = p.x - this.x;
    double deltaY = p.y - this.y;

    return sqrt(deltaX*deltaX + deltaY*deltaY);
  }

  // le type dynamic permet de définir n'importe quel type
  // on passe par le type Map pour s'appuyer sur une autre méthode existante
  Map<String, dynamic> toJson() {
    return {'x':x, 'y':y};
  }

  @override String toString() {
    return "Point($x, $y)";
  }
}