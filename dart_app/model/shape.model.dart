
import 'point.model.dart';

abstract class Shape {
  final Point p1;
  final Point p2;

  // Constructeur avec argument nommé
  // le mot clé required rend les attributs obligatoires
  // Point({required this.x, required this.y});
  // Constructeur const
  const Shape({
    required this.p1,
    required this.p2
  });

  double getPerimeter();
  double getArea();
  Map<String, dynamic> toJson();

}