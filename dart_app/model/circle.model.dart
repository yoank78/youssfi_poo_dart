
import 'dart:math';

import 'shape.model.dart';

class Circle extends Shape {
  
  // Circle({required super.p1, required super.p2});
  Circle({required p1, required p2}) : super(p1: p1, p2: p2);


  double getRadius() {
    // print("DEBUG : getRadius=${p1.distanceTo(p2)}");
    return p1.distanceTo(p2);
  }


  @override
  double getArea() {
    return getRadius()*getRadius()*pi;
  }

  @override
  double getPerimeter() {
    return 2*pi*getRadius();
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      'type':'Circle',
      'center':p1.toJson(),
      'radius':getRadius().toStringAsFixed(2)
    };
  }

}