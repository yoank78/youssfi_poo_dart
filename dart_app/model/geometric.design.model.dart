import 'dart:convert';
import 'dart:io';

import 'circle.model.dart';
import 'point.model.dart';
import 'rectangle.model.dart';
import 'shape.model.dart';

class GeometricDesign {
  List<Shape> shapes=[];
  
  GeometricDesign(){}

  GeometricDesign.fromFile(String filename) {
    File f = File(filename);
    String data = f.readAsStringSync();
    Map<String, dynamic> jsonData = json.decode(data);
    List<Shape> shapesList = (jsonData['shapes'] as List).map((item) {
      if (item['type']=='Circle'){
        double radius = double.parse(item['radius']);
        Point center = Point.fromJson(item['center']);
        return Circle(p1: center, p2: Point(x: center.x, y: center.y + radius));
      } else {
        double width = item['width'];
        double height = item['height'];
        Point p1 = Point(x: item['x'], y: item['y']);
        Point p2 = Point(x: p1.x + width, y: p1.y + height);
        return Rectangle(p1: p1, p2: p2);
      }
    }).toList();

    shapes=shapesList;
  }

  Shape add(Shape shape) {
    shapes.add(shape);
    return shape;
  }

  void show() {
    for(Shape shape in shapes) {
      if (shape is Circle) {
        print('******************* Circle *******************');
        print('Radius: ${shape.getRadius()}');
      } else if (shape is Rectangle) {
        print('******************* Rectangle *******************');
        print('Height: ${shape.getHeight()}');
        print('Width : ${shape.getWidth()}');
      }
      print(shape.toJson());
      print('Area  : ${shape.getArea()}');
      print('Perim : ${shape.getPerimeter()}');
    }
  }

  String toJson() {
    JsonEncoder jsonEncoder = const JsonEncoder.withIndent("  ");
    return jsonEncoder.convert({'shapes': shapes});
  }

  void save(String filename) {
    File f = new File(filename);
    f.writeAsStringSync(toJson());
    
  }
}