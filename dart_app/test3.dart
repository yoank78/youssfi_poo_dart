import 'model/circle.model.dart';
import 'model/point.model.dart';
import 'model/rectangle.model.dart';
import 'model/shape.model.dart';

void main() {
  print("Test3");

  Point p1 = const Point(x: 10, y: 20);
  Point p2 = const Point(x: 16, y: 32);

  // Il n'est pas possible d'instancier une classe abstraite
  // Shape shape=Shape(p1, p2);

  Shape c1 = new Circle(p1: p1, p2: p2);
  print("c1 : p1[${c1.p1.x}, ${c1.p1.y}], p2[${c1.p2.x}, ${c1.p2.y}]");
  print("Cercle   : ${c1.toJson()}");
  print("Area     : ${c1.getArea().toStringAsFixed(2)}");
  print("Perimeter: ${c1.getPerimeter().toStringAsFixed(2)}");

  // Pour accéder à la méthode getRadius à partir de notre objet Shape
  print("Radius     : ${(c1 as Circle).getRadius().toStringAsFixed(2)}");
  // OU
  if (c1 is Circle) {
    print("Radius     : ${c1.getRadius().toStringAsFixed(2)}");
  }

  print('=======================================');
  Point p5 = const Point(x: 10, y: 30);
  Point p6 = const Point(x: 15, y: 35);
  Rectangle r1 = Rectangle(p1: p5, p2: p6);

  print("Rectangle: ${r1.toJson()}");
  print("Area     : ${r1.getArea().toStringAsFixed(2)}");
  print("Perimeter: ${r1.getPerimeter().toStringAsFixed(2)}");
}