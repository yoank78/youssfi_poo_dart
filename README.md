# youssfi_poo_dart

## Project to try Dart language
Projet pour dérouler le lab : POO DART pour le Dév Mobile Flutter
Dart est le langage de programmation à utiliser pour faire du Flutter

Repo : https://github.com/mohamedYoussfi/poo-dart
- Part 1- POO DART pour le Dév Mobile Flutter : https://www.youtube.com/watch?v=lgvvmgeSFkY
- Part 2 - POO DART le Dév Mobile Flutter : https://www.youtube.com/watch?v=XIBQ9l4iQPQ
- Part 1- Flutter.start.best practices : https://www.youtube.com/watch?v=D-k_04hn4W8
- Part 2- Flutter.start.best practices - BloC : https://www.youtube.com/watch?v=iylnvLwObxg

- Part 1 - Dev Mobile avec Flutter - Graphisme
- https://www.youtube.com/watch?v=9SK5SDlGGuE

- Part 1- Flutter Mobile Developement - Fundamentals
- https://www.youtube.com/watch?v=DvAq5dKN5uk
- Part 2- Flutter Mobile Developement - REST API Interactions - Github Users and Repositories
- https://www.youtube.com/watch?v=oK8PTCvA61U

- Part 1 - Développement Mobile Natif et Cross Plateforme - Concepts de base
- https://www.youtube.com/watch?v=hAw64fsTLxU
- Part 2 - Développement Mobile Natif avec Android - Concepts de base
- https://www.youtube.com/watch?v=Tuw-4uJheYI
- Part 3 - Développement Mobile Natif avec Android - Première Application Android
- https://www.youtube.com/watch?v=eXD8AeFKDog
- Part 4 - Développement Mobile Natif avec Android - REST- Retrofit- Application Git Repositories API
- https://www.youtube.com/watch?v=uarFrYLAHLc
- Part 5 - Développement Mobile - FLUTTER Part 1
- https://www.youtube.com/watch?v=4MCmMQwkmB0
- Part 6 - Développement Mobile - FLUTTER Part 2
- https://www.youtube.com/watch?v=mrtTiFAsHUI
- Part 7 - Développement Mobile - FLUTTER Part 3
- https://www.youtube.com/watch?v=qyFIgJby0c0
- Part 8 - Developpement Mobile Flutter - Http - Rest API
- https://www.youtube.com/watch?v=wlwZrCEG0Q0
- Part 9 Développement Mobile Flutter - State Management - Provider & Blocs
- https://www.youtube.com/watch?v=6_LCplpMa4k
- Part 10 - Développement Mobile Flutter - Blocs - GET_IT DI - Contacts Messages App - Part 1
- https://www.youtube.com/watch?v=Du2-Nsxa6-8
- Part 11 - Développement Mobile Flutter - Blocs - GET_IT DI - Contacts Messages App - Part 2
- https://www.youtube.com/watch?v=Ac1Dw3mYWQw
- Part 12 - Développement Mobile Flutter - Blocs - GET_IT DI - Contacts Messages App - Part 3
- https://www.youtube.com/watch?v=UFwO__m0-pY
- Part 13 - Développement Mobile Flutter - Blocs - GET_IT DI - Contacts Messages App - Part 4
- https://www.youtube.com/watch?v=LDgn-QRtWU0


## Resources
- https://www.gitpod.io/docs/languages/dart#dart
- https://www.gitpod.io/blog/gitpodify
- https://dartcode.org/
- https://docs.flutter.dev/development/tools/vs-code
- https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code
- https://flutterawesome.com/demonstrate-how-to-easily-program-android-apps-on-gitpod/
- https://flutterrepos.com/lib/JamesCullum-flutter-adb-template-flutter-example-apps

Resources sur le repo GitHub :
This project is a starting point for a Flutter application.
A few resources to get you started if this is your first Flutter project:

- Lab: Write your first Flutter app
- https://flutter.dev/docs/get-started/codelab
- Cookbook: Useful Flutter samples
- https://flutter.dev/docs/cookbook
- For help getting started with Flutter, view our online documentation, which offers tutorials, samples, guidance on mobile development, and a full API reference.
- https://flutter.dev/docs

Autres resources:
- https://github.com/vtorres/gitpod-flutter
- https://gist.github.com/estevez-dev/878885e2eb236a37b0f6fa70dd2928ec
- https://madewithflutter.net/best-flutter-tools/
- David Silvera : https://www.youtube.com/c/DavidSilveraChannel/videos
- Apprendre à coder pour les vrais débutants avec Dart et Flutter : https://www.youtube.com/watch?v=Nc8Rlz1tjH8
- Playlist : Conseils et astuces Dart & Flutter : https://www.youtube.com/watch?v=MPNaIJayn_A&list=PLVaasf-927w7aGsGOP0aYNOcK36bhxvhn
- Playlist : Formation Flutter - Créer une application Android et iOS : https://www.youtube.com/playlist?list=PLVaasf-927w4T1f42loBJDEYbQP1wd9VB
- Playlist : Architecture & Flutter: Comment organiser votre code : https://www.youtube.com/watch?v=tUtM88-L_vQ&list=PLVaasf-927w6ByeuIx5Ozp6-Xs2bM5XZD

## Parcours des vidéos
Part1 : https://www.youtube.com/watch?v=lgvvmgeSFkY
00:13:20 - version de flutter => flutter --version
pour l'instant, l'image ne contient pas flutter, juste dart
00:16:00 - Test 1er programme dans : /workspace/youssfi_poo_dart/dart_app/main.dart + execution depuis ce répertoire : dart main.dart
00:34:30 - Utilisation constructeur avec attributs nommés
00:41:00 - Surcharge de la méthode toString()
00:42:00 - Constructeur nommé
00:46:30 - Ajout méthode calcul de distance entre 2 points
00:52:50 - Méthode pour convertir en format JSON
01:00:00 - Nouveau constructeur avec un JSON en entrée
01:13:00 - Constructeur constant

Part2 : POO DART le Dév Mobile Flutter
https://www.youtube.com/watch?v=XIBQ9l4iQPQ
00:00:00 - Création class Rectangle
00:10:50 - Création class GeometricDesign
00:30:00 - Ecriture dans un fichier


...
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/yoank78/youssfi_poo_dart.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/yoank78/youssfi_poo_dart/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
